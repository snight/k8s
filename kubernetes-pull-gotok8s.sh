#!/bin/bash
MY_REGISTRY=gotok8s
K8S_VERSION="1.20.0"

echo ""
echo "=========================================================="
echo "Pull Kubernetes for x64 v$K8S_VERSION Images from gotok8s ......"
echo "=========================================================="
echo ""

## 拉取镜像
docker pull ${MY_REGISTRY}/kube-apiserver:v$K8S_VERSION
docker pull ${MY_REGISTRY}/kube-controller-manager:v$K8S_VERSION
docker pull ${MY_REGISTRY}/kube-scheduler:v$K8S_VERSION
docker pull ${MY_REGISTRY}/kube-proxy:v$K8S_VERSION
docker pull ${MY_REGISTRY}/etcd:3.4.13-0
docker pull ${MY_REGISTRY}/pause:3.2
docker pull coredns/coredns:1.7.0

## 添加Tag
docker tag ${MY_REGISTRY}/kube-apiserver:v$K8S_VERSION k8s.gcr.io/kube-apiserver:v$K8S_VERSION
docker tag ${MY_REGISTRY}/kube-scheduler:v$K8S_VERSION k8s.gcr.io/kube-scheduler:v$K8S_VERSION
docker tag ${MY_REGISTRY}/kube-controller-manager:v$K8S_VERSION k8s.gcr.io/kube-controller-manager:v$K8S_VERSION
docker tag ${MY_REGISTRY}/kube-proxy:v$K8S_VERSION k8s.gcr.io/kube-proxy:v$K8S_VERSION
docker tag ${MY_REGISTRY}/etcd:3.4.13-0 k8s.gcr.io/etcd:3.4.13-0
docker tag ${MY_REGISTRY}/pause:3.2 k8s.gcr.io/pause:3.2
docker tag coredns/coredns:1.7.0 k8s.gcr.io/coredns:1.7.0

echo ""
echo "=========================================================="
echo "Pull Kubernetes for x64 v$K8S_VERSION Images FINISHED."
echo "into gotok8s, "
echo "=========================================================="
echo ""
